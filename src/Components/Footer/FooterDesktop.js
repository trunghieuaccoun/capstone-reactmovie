import React from 'react';

export default function FooterDesktop() {
    return (
        <div>
            <div className='bg-gray-50 border-t fixed bottom-0 w-screen z-10  py-3 hidden md:block'>
                <div className='container mx-auto px-10 flex justify-between items-center text-gray-500 '>
                    <div>
                        <span>© 2022 Movie, Inc.</span>
                        <span className='px-3 hover:underline cursor-pointer'>
                            Quyền riêng tư
                        </span>
                        .
                        <span className='px-3 hover:underline cursor-pointer'>
                            Điều khoản
                        </span>
                        .
                        <span className='px-3 hover:underline cursor-pointer'>
                            Sơ đồ trang web
                        </span>
                        .
                    </div>
                    <div className='text-gray-700'>
                        <i className='fa-solid fa-earth-oceania'></i>
                        <span className='hover:underline cursor-pointer px-3 font-medium'>
                            Tiếng Việt(VN)
                        </span>
                        <i className='fa fa-dollar-sign font-medium cursor-pointer'></i>
                        <span className='hover:underline cursor-pointer px-2 font-medium'>
                            USD
                        </span>
                        <span className='font-medium hover:underline'>
                            Hỗ trợ tài nguyên <i className='fa fa-angle-up'></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    );
}
