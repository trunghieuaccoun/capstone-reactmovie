import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Card } from 'antd';
import { getMovieById } from './../../service/movieService';
import moment from 'moment';
import 'moment';
import 'moment/locale/vi';
moment.locale('vi');
const { Meta } = Card;
export default function DetailPage() {
    let params = useParams();
    const [movieID, setMovieID] = useState([]);
    useEffect(() => {
        getMovieById(params.id)
            .then((res) => {
                console.log(res);
                setMovieID(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);
    const renderDetail = () => {
        return (
            <div className='container mx-auto'>
                <div className='flex justify-center'>
                    <div>
                        <img
                            src={movieID.hinhAnh}
                            alt='exp'
                            className='object-cover w-96 h-96'
                        />
                    </div>
                    <Card title={movieID.tenPhim} style={{ width: 300 }}>
                        <span className='bg-red-500 text-white px-3 py-2 rounded font-medium'>
                            {moment(movieID.ngayKhoiChieu).format(
                                'DD/MM hh:mm A'
                            )}
                        </span>

                        <p
                            className='py-3 

                        '
                        >
                            Giới thiệu phim :{movieID.moTa}
                        </p>
                        <p>Đánh giá của chuyên gia :{movieID.danhGia}</p>
                    </Card>
                </div>
            </div>
        );
    };
    return <div className='container'>{renderDetail()}</div>;
}
