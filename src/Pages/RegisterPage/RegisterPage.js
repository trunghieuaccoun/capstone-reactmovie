import React from 'react';
import bg_animate_register from '../../assets/101191-submit-application-successfully.json';
import Lottie from 'lottie-react';
import { Button, Form, Input, message } from 'antd';
import { postRegister } from '../../service/userService';
const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};

/* eslint-disable no-template-curly-in-string */
const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not a valid email!',
        number: '${label} is not a valid number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};
export default function RegisterPage() {
    const onFinish = (values) => {
        postRegister(values)
            .then((res) => {
                console.log(res);
                message.success('Đăng ký thành công');
            })
            .catch((err) => {
                message.error('Đăng ký thất bại');
                console.log(err);
            });
    };
    return (
        <div className='container p-5 rounded bg-white flex'>
            <div className='w-1/2'>
                <Lottie animationData={bg_animate_register} loop={true} />
            </div>
            <div className='w-1/2'>
                <Form
                    {...layout}
                    name='nest-messages'
                    onFinish={onFinish}
                    validateMessages={validateMessages}
                >
                    <Form.Item
                        name={['user', 'taiKhoan']}
                        label='tài khoản'
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name={['user', 'matKhau']}
                        label='mật khẩu'
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name={['user', 'soDt']}
                        label='số điện thoại'
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name={['user', 'maNhom']}
                        label='mã nhóm'
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name={['user', 'hoTen']}
                        label='họ tên'
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name={['user', 'email']}
                        label='Email'
                        rules={[{ required: true, type: 'email' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{
                            ...layout.wrapperCol,
                            offset: 12,
                        }}
                    >
                        <Button
                            className='bg-blue-500 hover:text-white'
                            htmlType='submit'
                        >
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
}
