import React from 'react';
import { Button, Form, Input, message } from 'antd';
import { postLogin } from '../../service/userService';
import { useNavigate } from 'react-router-dom';
import { SET_USER_LOGIN } from '../../redux/constant/userContant';
import { userLocalService } from '../../service/localService';
import { useDispatch } from 'react-redux';
import Lottie from 'lottie-react';
import bg_animate from '../../assets/26436-login-circle.json';
export default function LoginPage() {
    let navigate = useNavigate();
    let dispatch = useDispatch();
    const onFinish = (values) => {
        console.log('Success:', values);
        postLogin(values)
            .then((res) => {
                console.log(res);
                //
                message.success('Đăng nhập thành công');
                dispatch({
                    type: SET_USER_LOGIN,
                    payload: res.data.content,
                });
                userLocalService.set(res.data.content);
                setTimeout(() => {
                    navigate('/');
                }, 1000);
            })
            .catch((err) => {
                message.error('Đăng nhập thất bại');
                console.log(err);
            });
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div className='w-screen h-screen flex justify-center items-center bg-blue-500'>
            <div className='container p-5 rounded bg-white flex'>
                <div className='w-1/2'>
                    <Lottie animationData={bg_animate} loop={true} />
                </div>
                <div className='w-1/2 pt-28'>
                    <Form
                        layout='vertical'
                        name='basic'
                        labelCol={{
                            span: 8,
                        }}
                        wrapperCol={{
                            span: 24,
                        }}
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete='off'
                    >
                        <Form.Item
                            label='Username'
                            name='taiKhoan'
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your username!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label='Password'
                            name='matKhau'
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item
                            wrapperCol={{
                                span: 24,
                            }}
                            className='text-center'
                        >
                            <Button
                                className='bg-blue-500 hover:text-white'
                                htmlType='submit'
                            >
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div>
    );
}
